import tkinter as tk
import tkinter.font as tkFont


class CustomEntry(tk.Entry):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config(font=self._custom_font(15), width=3)
        self.value = tk.StringVar()
        self.config(textvariable=self.value)

        self.config(validate='focusout', validatecommand=self._validate)
        self.bind('<Return>', func=self._validate)

    def _validate(self, *args):
        """ Callback to start validation, must be implemented before called """
        raise NotImplementedError

    def onValidationSuccess(self):
        """ Callback for when validation succeeds, must be implemented before called """
        raise NotImplementedError

    def onValidationFail(self):
        """ Callback for when validation fails, must be implemented before called """
        raise NotImplementedError

    @staticmethod
    def _custom_font(size):
        return tkFont.Font(family='Helvetica', size=size)

    def increment(self):
        old_val = int(self.get())
        new_val_idx = (self.possible_values.index(old_val) + 1) % len(self.possible_values)
        self.delete(0, 'end')
        self.insert(0, self.possible_values[new_val_idx])
        self.onValidationSuccess()

    def decrement(self):
        old_val = int(self.get())
        new_val_idx = (self.possible_values.index(old_val) - 1) % len(self.possible_values)
        self.delete(0, 'end')
        self.insert(0, self.possible_values[new_val_idx])
        self.onValidationSuccess()
