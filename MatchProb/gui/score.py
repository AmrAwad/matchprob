import tkinter as tk
import tkinter.font as tkFont
from .custom_entry import CustomEntry
from .custom_button import CustomButton


class GameScoreEntry(CustomEntry):
    def _validate(self, *args):
        new_val = self.value.get()
        try:
            if int(new_val) in self.possible_values:
                self.onValidationSuccess()
            else:
                self.OnValidationFail(new_val)
        except ValueError as e:
            self.OnValidationFail(new_val)

    def OnValidationFail(self, invalid_val):
        self.bell()
        error_msg = "{} is not a valid game score! valid game scores are [{}]" \
                    .format(invalid_val, ', '.join(map(str, self.possible_values)))
        print(error_msg)

    @property
    def possible_values(self):
        return list(range(9))


class SetScoreEntry(CustomEntry):
    def _validate(self, *args):
        new_val = self.value.get()
        try:
            if 0 <= int(new_val) <= 7:
                self.onValidationSuccess()
            else:
                self.OnValidationFail(new_val)
        except ValueError as e:
            self.OnValidationFail(new_val)

    def OnValidationFail(self, invalid_val):
        self.bell()
        error_msg = "{} is not a valid set score! valid set scores are 0..7".format(invalid_val)
        print(error_msg)

    @property
    def possible_values(self):
        return list(range(8))


class MatchScoreEntry(CustomEntry):
    def _validate(self, *args):
        new_val = self.value.get()
        try:
            if 0 <= int(new_val) <= self.set_type.get():
                self.onValidationSuccess()
            else:
                self.OnValidationFail(new_val)
        except ValueError as e:
            self.OnValidationFail(new_val)

    def OnValidationFail(self, invalid_val):
        self.bell()
        error_msg = "{} is not a valid match score! valid set scores are 0..{}" \
                    .format(invalid_val, self.set_type.get())
        print(error_msg)

    @property
    def possible_values(self):
        return list(range(self.set_type.get() + 1))


class ScoreFrame(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master, highlightbackground="black", highlightcolor="black", highlightthickness=1)
        self.grid()

        self._create_widgets()
        self._adjust_layout()

    def _create_widgets(self):

        tk.Label(self, text="Hero", font=self._custom_font(20)).grid(row=1, rowspan=2, column=1, padx=10, pady=10)
        tk.Label(self, text="Villain", font=self._custom_font(20)).grid(row=3, rowspan=2, column=1, padx=10, pady=10)

        tk.Label(self, text="Points", font=self._custom_font(20)).grid(row=0, column=2, padx=10, pady=10, columnspan=2)

        self.server_game_score = GameScoreEntry(self)
        self.server_game_score.insert(0, 0)

        self.server_game_score_increment = CustomButton(self, text='+',
                                                        command=self.server_game_score.increment)

        self.server_game_score_decrement = CustomButton(self, text='-',
                                                        command=self.server_game_score.decrement)

        self.returner_game_score = GameScoreEntry(self)
        self.returner_game_score.insert(0, 0)

        self.returner_game_score_increment = CustomButton(self, text='+',
                                                          command=self.returner_game_score.increment)

        self.returner_game_score_decrement = CustomButton(self, text='-',
                                                          command=self.returner_game_score.decrement)

        tk.Label(self, text="Games", font=self._custom_font(20)).grid(row=0, column=4, padx=10, pady=10, columnspan=2)
        self.server_set_score = SetScoreEntry(self)
        self.server_set_score.insert(0, 0)

        self.server_set_score_increment = CustomButton(self, text='+', command=self.server_set_score.increment)
        self.server_set_score_decrement = CustomButton(self, text='-', command=self.server_set_score.decrement)

        self.returner_set_score = SetScoreEntry(self)
        self.returner_set_score.insert(0, 0)

        self.returner_set_score_increment = CustomButton(self, text='+', command=self.returner_set_score.increment)
        self.returner_set_score_decrement = CustomButton(self, text='-', command=self.returner_set_score.decrement)

        tk.Label(self, text="Sets", font=self._custom_font(20)).grid(row=0, column=6, padx=10, pady=10, columnspan=2)
        self.server_match_score = MatchScoreEntry(self)
        self.server_match_score.insert(0, 0)

        self.server_match_score_increment = CustomButton(self, text='+', command=self.server_match_score.increment)
        self.server_match_score_decrement = CustomButton(self, text='-', command=self.server_match_score.decrement)

        self.returner_match_score = MatchScoreEntry(self)
        self.returner_match_score.insert(0, 0)

        self.returner_match_score_increment = CustomButton(self, text='+', command=self.returner_match_score.increment)
        self.returner_match_score_decrement = CustomButton(self, text='-', command=self.returner_match_score.decrement)

    def _adjust_layout(self):
        self.server_game_score.grid(row=1, rowspan=2, column=2, padx=(10, 0), pady=(10, 15))
        self.server_game_score_increment.grid(row=1, column=3)
        self.server_game_score_decrement.grid(row=2, column=3)

        self.returner_game_score.grid(row=3, rowspan=2, column=2, padx=(10, 0), pady=(10, 15))
        self.returner_game_score_increment.grid(row=3, column=3)
        self.returner_game_score_decrement.grid(row=4, column=3)

        self.server_set_score.grid(row=1, rowspan=2, column=4, padx=(10, 0), pady=(10, 15))
        self.server_set_score_increment.grid(row=1, column=5, padx=(10, 0))
        self.server_set_score_decrement.grid(row=2, column=5, padx=(10, 0))

        self.returner_set_score.grid(row=3, rowspan=2, column=4, padx=(10, 0), pady=(10, 15))
        self.returner_set_score_increment.grid(row=3, column=5, padx=(10, 0))
        self.returner_set_score_decrement.grid(row=4, column=5, padx=(10, 0))

        self.server_match_score.grid(row=1, rowspan=2, column=6, padx=(10, 0), pady=(10, 15))
        self.server_match_score_increment.grid(row=1, column=7)
        self.server_match_score_decrement.grid(row=2, column=7)

        self.returner_match_score.grid(row=3, rowspan=2, column=6, padx=(10, 0), pady=(10, 15))
        self.returner_match_score_increment.grid(row=3, column=7)
        self.returner_match_score_decrement.grid(row=4, column=7)

    # TODO: Remove this from this class
    @staticmethod
    def _custom_font(size):
        return tkFont.Font(family='Helvetica', size=size)
