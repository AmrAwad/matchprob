import tkinter as tk
from functools import partial
from .results import Results
from .inputs import Inputs
from .score import ScoreFrame
from ..algorithm import matchProb


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self._create_subframes()
        self._bind_functions()
        self.update_results()

    def _create_subframes(self):
        self.grid()

        self.results = Results(self)
        self.results.grid(row=0, column=0, padx=20, pady=20, columnspan=2)

        self.inputs = Inputs(self)
        self.inputs.grid(row=1, column=0, padx=20, pady=20)

        self.score = ScoreFrame(self)
        self.score.grid(row=1, column=1, padx=20, pady=20)

    def _bind_functions(self):
        self.inputs.server_prob.onValidationSuccess = self.update_results
        self.inputs.returner_prob.onValidationSuccess = self.update_results

        self.inputs.tiebreak.trace_add("write", self.update_results)
        self.inputs.set_type.trace_add("write", self.update_results)

        self.score.server_game_score.onValidationSuccess = self.update_results
        self.score.returner_game_score.onValidationSuccess = self.update_results

        self.score.server_set_score.onValidationSuccess = self.update_results
        self.score.returner_set_score.onValidationSuccess = self.update_results

        self.score.server_match_score.onValidationSuccess = self.update_results
        self.score.server_match_score.set_type = self.inputs.set_type

        self.score.returner_match_score.onValidationSuccess = self.update_results
        self.score.returner_match_score.set_type = self.inputs.set_type

    def update_results(self, *args):
        server_game_score = int(self.score.server_game_score.get())
        returner_game_score = int(self.score.returner_game_score.get())

        calc_match_prob = partial(matchProb, 
                                  server_wins_service_prob=float(self.inputs.server_prob.get()) / 100,
                                  server_wins_return_prob=float(self.inputs.returner_prob.get()) / 100,
                                  server_set_score=int(self.score.server_set_score.get()),
                                  returner_set_score=int(self.score.returner_set_score.get()),
                                  server_match_score=int(self.score.server_match_score.get()),
                                  returner_match_score=int(self.score.returner_match_score.get()),
                                  sets=int(self.inputs.set_type.get()))

        server_win_prob = calc_match_prob(server_game_score=server_game_score,
                                          returner_game_score=returner_game_score)

        server_win_prob_plus_1 = calc_match_prob(server_game_score=server_game_score+1,
                                                 returner_game_score=returner_game_score)

        server_game_score_minus_1 = server_game_score - 1 if server_game_score > 0 else 0
        server_win_prob_minus_1 = calc_match_prob(server_game_score=server_game_score_minus_1,
                                                  returner_game_score=returner_game_score)

        self.results.server_win_prob['text'] = "{}%".format(round(server_win_prob * 100, 2))
        self.results.returner_win_prob['text'] = "{}%".format(round((1 - server_win_prob) * 100, 2))

        self.results.server_win_odds['text'] = self.prob_to_odds(1 - server_win_prob)
        self.results.returner_win_odds['text'] = self.prob_to_odds(server_win_prob)

        self.results.server_win_odds_plus_1['text'] = self.prob_to_odds(1 - server_win_prob_plus_1)
        self.results.returner_win_odds_plus_1['text'] = self.prob_to_odds(server_win_prob_plus_1)

        self.results.server_win_prob_plus_1['text'] = "{}%".format(round(server_win_prob_plus_1 * 100, 2))
        self.results.returner_win_prob_plus_1['text'] = "{}%".format(round((1 - server_win_prob_plus_1) * 100, 2))

        self.results.server_win_odds_minus_1['text'] = self.prob_to_odds(1 - server_win_prob_minus_1)
        self.results.returner_win_odds_minus_1['text'] = self.prob_to_odds(server_win_prob_minus_1)

        self.results.server_win_prob_minus_1['text'] = "{}%".format(round(server_win_prob_minus_1 * 100, 2))
        self.results.returner_win_prob_minus_1['text'] = "{}%".format(round((1 - server_win_prob_minus_1) * 100, 2))

    @staticmethod
    def prob_to_odds(prob):
        if prob != 1:
            return round(1 / (1 - prob), 2)
        else:
            return '-'

    @classmethod
    def run(cls):
        root = tk.Tk()
        app = cls(master=root)

        app.master.title("MatchProb")
        app.mainloop()
