import tkinter as tk
import tkinter.font as tkFont
from .custom_entry import CustomEntry
from .custom_button import CustomButton


class ProbEntry(CustomEntry):
    def _validate(self, *args):
        new_val = self.value.get()
        try:
            if 0 <= float(new_val) <= 100:
                self.onValidationSuccess()
            else:
                self.OnValidationFail(new_val)
        except ValueError as e:
            self.OnValidationFail(new_val)

    def OnValidationFail(self, invalid_val):
        self.bell()
        error_msg = "{} is not a valid probability! valid probabilities are 0..100" \
                    .format(invalid_val)
        print(error_msg)

    def increment(self):
        old_val = float(self.get())
        self.delete(0, 'end')
        self.insert(0, (old_val+1) % 100)
        self.onValidationSuccess()

    def decrement(self):
        old_val = float(self.get())
        self.delete(0, 'end')
        self.insert(0, (old_val-1) % 100)
        self.onValidationSuccess()


class Inputs(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master, highlightbackground="black", highlightcolor="black", highlightthickness=1)
        self.grid()

        self.set_type = tk.IntVar(value=3)
        self.tiebreak = tk.IntVar(value=True)

        self._create_widgets()
        self._adjust_layout()

    def _create_widgets(self):
        tk.Label(self, text="P (serve)", font=self._custom_font(15)).grid(row=0, rowspan=2, column=0, padx=20, pady=10)
        self.server_prob = ProbEntry(self, width=8)
        self.server_prob.insert(0, '0')

        self.server_prob_increment = CustomButton(self, text='+', command=self.server_prob.increment)
        self.server_prob_decrement = CustomButton(self, text='-', command=self.server_prob.decrement)

        tk.Label(self, text="P (return)", font=self._custom_font(15)).grid(row=2, rowspan=2, column=0, padx=20, pady=10)
        self.returner_prob = ProbEntry(self, width=8)
        self.returner_prob.insert(0, '0')

        self.returner_prob_increment = CustomButton(self, text='+', command=self.returner_prob.increment)
        self.returner_prob_decrement = CustomButton(self, text='-', command=self.returner_prob.decrement)

        tk.Label(self, text="Number\nOf Sets", font=self._custom_font(15)).grid(row=4, column=0, padx=20, pady=10, rowspan=2)
        tk.Radiobutton(self, text="3 Sets", variable=self.set_type, value=3).grid(row=4, column=1, padx=20, columnspan=2)
        tk.Radiobutton(self, text="5 Sets", variable=self.set_type, value=5).grid(row=5, column=1, padx=20, columnspan=2)

        tk.Label(self, text="Tie break", font=self._custom_font(15)).grid(row=6, column=0, padx=20, pady=10)
        tk.Checkbutton(self, text="True", variable=self.tiebreak).grid(row=6, column=1, padx=20, pady=10, columnspan=2)

    def _adjust_layout(self):
        self.server_prob.grid(row=0, rowspan=2, column=1, padx=(10, 0), pady=(10, 15))
        self.server_prob_increment.grid(row=0, column=2)
        self.server_prob_decrement.grid(row=1, column=2)

        self.returner_prob.grid(row=2, rowspan=2, column=1, padx=(10, 0), pady=(10, 15))
        self.returner_prob_increment.grid(row=2, column=2)
        self.returner_prob_decrement.grid(row=3, column=2)


    @staticmethod
    def _custom_font(size):
        return tkFont.Font(family='Helvetica', size=size)
