import tkinter as tk
import tkinter.font as tkFont


class Results(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master, highlightbackground="black", highlightcolor="black", highlightthickness=1)
        self.grid()

        self.create_widgets()

    def create_widgets(self):
        tk.Label(self, text="Match\nOdds", font=self._custom_font(25)).grid(row=0, column=0, padx=20, rowspan=2)

        self.server_win_odds = tk.Label(self, font=self._custom_font(22), width=6)
        self.server_win_odds.grid(row=0, column=1, padx=20, pady=10)
        self.returner_win_odds = tk.Label(self, font=self._custom_font(22), width=6)
        self.returner_win_odds.grid(row=1, column=1, padx=20, pady=10)

        self.server_win_prob = tk.Label(self, font=self._custom_font(14), width=6)
        self.server_win_prob.grid(row=0, column=2, pady=10)
        self.returner_win_prob = tk.Label(self, font=self._custom_font(14), width=6)
        self.returner_win_prob.grid(row=1, column=2, pady=10)

        tk.Label(self, text="+1\nPoint", font=self._custom_font(25)).grid(row=0, column=3, padx=20, rowspan=2)

        self.server_win_odds_plus_1 = tk.Label(self, font=self._custom_font(22), width=6)
        self.server_win_odds_plus_1.grid(row=0, column=4, padx=20, pady=10)
        self.returner_win_odds_plus_1 = tk.Label(self, font=self._custom_font(22), width=6)
        self.returner_win_odds_plus_1.grid(row=1, column=4, padx=20, pady=10)

        self.server_win_prob_plus_1 = tk.Label(self, font=self._custom_font(14), width=6)
        self.server_win_prob_plus_1.grid(row=0, column=5, pady=10)
        self.returner_win_prob_plus_1 = tk.Label(self, font=self._custom_font(14), width=6)
        self.returner_win_prob_plus_1.grid(row=1, column=5, pady=10)

        tk.Label(self, text="-1\nPoint", font=self._custom_font(25)).grid(row=0, column=6, padx=20, rowspan=2)

        self.server_win_odds_minus_1 = tk.Label(self, font=self._custom_font(22), width=6)
        self.server_win_odds_minus_1.grid(row=0, column=7, padx=20, pady=10)
        self.returner_win_odds_minus_1 = tk.Label(self, font=self._custom_font(22), width=6)
        self.returner_win_odds_minus_1.grid(row=1, column=7, padx=20, pady=10)

        self.server_win_prob_minus_1 = tk.Label(self, font=self._custom_font(14), width=6)
        self.server_win_prob_minus_1.grid(row=0, column=8, pady=10)
        self.returner_win_prob_minus_1 = tk.Label(self, font=self._custom_font(14), width=6)
        self.returner_win_prob_minus_1.grid(row=1, column=8, pady=10)


    @staticmethod
    def _custom_font(size):
        return tkFont.Font(family='Helvetica', size=size)
