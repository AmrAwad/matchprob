## calculates probability of winning a tennis match from any given score dependent on the skill levels
## of the two players
 
from .GameProb import gameProb
from .SetProb import setGeneral
from .TiebreakProb import tiebreakProb
 
def fact(x):
    if x in [0, 1]:  return 1
    r = 1
    for a in range(1, (x+1)):  r = r*a
    return r
 
def ch(a, b):
    return fact(a)/(fact(b)*fact(a-b))
 
def matchGeneral(e, v=0, w=0, s=3):
    '''
    calculates probability of winning the match from the beginning of a set
    params:
        e is p(winning a set)
        v and w is current set score
        s is total number of sets ("best of")
    '''
    towin = (s+1)//2
    left = towin - v
    if left == 0:   return 1
    remain = s - v - w
    if left > remain:   return 0
    win = 0
    for i in range(left, (remain+1)):
        add = ch((i-1), (left-1))*(e**(left-1))*((1-e)**(i-left))*e
        win += add
    return win
 
def matchProb(server_wins_service_prob, server_wins_return_prob, server_game_score=0, returner_game_score=0,
              server_set_score=0, returner_set_score=0, server_match_score=0, returner_match_score=0, sets=3):
    '''
    calculates probability of winning a match from any given score,
    params:
        s, server_wins_return_prob: p(server wins a service point), p(server wins return point)
        server_game_score, returner_game_score: current score within the game. e.g. 30-15 is 2, 1
        server_set_score, returner_set_score: current score within the set. e.g. 5, 4
        server_match_score, returner_match_score: current score within the match (number of sets for each player)
        v's are serving player; w's are returning player
        sets: "best of", so default is best of 3
    '''
    a = gameProb(server_wins_service_prob)
    b = gameProb(server_wins_return_prob)
    c = setGeneral(server_wins_service_prob, server_wins_return_prob)
    if server_game_score == 0 and returner_game_score == 0: ## no point score
        if server_set_score == 0 and returner_set_score == 0: ## no game score
            return matchGeneral(c, v=server_match_score, w=returner_match_score, s=sets)
        else:   ## we're in mid-set, no point score
            sWin = setGeneral(a, b, v=server_set_score, w=returner_set_score)
            sLoss = 1 - sWin
    elif server_set_score == 6 and returner_set_score == 6:         
        sWin = tiebreakProb(server_wins_service_prob, server_wins_return_prob, v=server_game_score, w=returner_game_score)
        sLoss = 1 - sWin       
    else:
        gWin = gameProb(server_wins_service_prob, v=server_game_score, w=returner_game_score)
        gLoss = 1 - gWin
        sWin = gWin*(1 - setGeneral((1-server_wins_return_prob), (1-server_wins_service_prob), v=returner_set_score, w=(server_set_score+1)))
        sWin += gLoss*(1 - setGeneral((1-server_wins_return_prob), (1-server_wins_service_prob), v=(returner_set_score+1), w=server_set_score))
        sLoss = 1 - sWin
    mWin = sWin*matchGeneral(c, v=(server_match_score+1), w=returner_match_score, s=sets)
    mWin += sLoss*matchGeneral(c, v=server_match_score, w=(returner_match_score+1), s=sets)
    return mWin