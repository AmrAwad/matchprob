import os
import csv
import unittest
from MatchProb import matchProb, gameProb, setGeneral, tiebreakProb

class TestMatch(unittest.TestCase):
    def test_matchProb(self):
        with open(os.path.join(os.path.dirname(__file__), 'MatchProb_test.csv'), 'r', encoding='utf-8-sig') as fh:
            tests = csv.reader(fh, delimiter=';')

            # Skipping header file
            next(tests)

            for test in tests:
                # Formatting inputs
                server_wins_service_prob = float(test[0].replace(',', '.'))
                server_wins_return_prob = float(test[1].replace(',', '.'))
                server_game_score = int(test[2])
                returner_game_score = int(test[3])
                server_set_score = int(test[4])
                returner_set_score = int(test[5])
                server_match_score = int(test[6])
                returner_match_score = int(test[7])
                sets = int(test[8])
                output = float(test[9].replace(',', '.'))
                self.assertEqual(output, matchProb(server_wins_service_prob=server_wins_service_prob,
                                                   server_wins_return_prob=server_wins_return_prob,
                                                   server_game_score=server_game_score,
                                                   returner_game_score=returner_game_score,
                                                   server_set_score=server_set_score,
                                                   returner_set_score=returner_set_score,
                                                   server_match_score=server_match_score,
                                                   returner_match_score=returner_match_score, sets=sets))

    def test_gameProb(self):
        with open(os.path.join(os.path.dirname(__file__), 'Gameprob_test.csv'), 'r', encoding='utf-8-sig') as fh:
            tests = csv.reader(fh, delimiter=';')

            # Skipping header file
            next(tests)

            for test in tests:
                # Formatting inputs
                s = float(test[0].replace(',', '.'))
                v = int(test[1])
                w = int(test[2])
                output = float(test[3].replace(',', '.'))

                self.assertEqual(output, round(gameProb(s=s, v=v, w=w), 9))

    def test_setGeneral(self):
        with open(os.path.join(os.path.dirname(__file__), 'setProb_test.csv'), 'r', encoding='utf-8-sig') as fh:
            tests = csv.reader(fh, delimiter=';')

            # Skipping header file
            next(tests)

            for test in tests:
                s = float(test[0].replace(',', '.'))
                u = float(test[1].replace(',', '.'))
                v = int(test[2])
                w = int(test[3])
                tb = int(test[4])
                self.assertEqual(float(test[5].replace(',', '.')), setGeneral(s=s, u=u, v=v, w=w, tb=tb))


    def test_tiebreakProb(self):
        with open(os.path.join(os.path.dirname(__file__), 'tiebreakProb_test.csv'), 'r', encoding='utf-8-sig') as fh:
            tests = csv.reader(fh, delimiter=';')

            # Skipping header file
            next(tests)

            for test in tests:
                # Formatting inputs
                s = float(test[0].replace(',', '.'))
                t = float(test[1].replace(',', '.'))
                v = int(test[2])
                w = int(test[3])
                p = int(test[4])
                output = float(test[5].replace(',', '.'))

                self.assertEqual(output, round(tiebreakProb(s=s, t=t, v=v, w=w, p=p), 9))
