import coverage
import unittest

COV = coverage.coverage(branch=True, include='MatchProb/*')
COV.start()

suite = unittest.TestLoader().discover('tests')
unittest.TextTestRunner(verbosity=2).run(suite)

COV.stop()
COV.report()
