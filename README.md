# MatchProb: Tennis match results predictor

This application aims to predict the output of a tennis match based on the following inputs:

1. probability to win a point on serve
2. probability to win a point on return
3. Current score
4. Game type (3-set / 5-set)
5. Tiebreak

The project uses Python3.

## Running the GUI
```
python main.py
```

## Packaging the application
1. Install `pyinstaller`
```
pip install pyinstaller
```
2. Package the application into a single executable
```
pyinstaller main.py -n matchprob --onefile
```
If you'd like the final executable file to not have a console, add the option `--noconsole`
```
pyinstaller main.py -n matchprob --onefile --noconsole
```

The final executable will be available under `dist` folder.